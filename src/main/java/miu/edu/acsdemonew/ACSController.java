package miu.edu.acsdemonew;


import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@Component
@RestController
public class ACSController {

    @GetMapping("/test/{name}")
    public String getName(@PathVariable String name){
        return "ACS: Hello "+ name;
    }
}
