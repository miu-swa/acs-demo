package miu.edu.acsdemonew;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AcsDemoNewApplication {

    public static void main(String[] args) {
        SpringApplication.run(AcsDemoNewApplication.class, args);
    }

}
